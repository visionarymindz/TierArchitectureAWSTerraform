variable "vpc_cidr_block" {
  default = "10.0.0.0/24"
}

variable "public_subnet_cidr_block" {
  default = "10.0.0.0/25"
}

variable "private_subnet_cidr_block" {
  default = "10.0.0.128/25"
}

variable "bucket_name" {
  type = string
}

variable "iam_role_name" {
  type = string
}

